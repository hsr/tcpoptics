#!/usr/bin/python

import sys, os, re
from boomslang import *

if len(sys.argv) < 3:
    print "Usage: %s <filter> <output_dir>\n" % sys.argv[0]
    print "       where filter is part of the filename."
    print "       Inform multiple filters separating them with ,"
    print "       output_dir is the directory where the script will"
    print "       put the graphs."
    sys.exit(0)

filters = sys.argv[1].split(',')
output_dir = sys.argv[2]

# plot = Plot()
# data = dict()

# filenames = []
# time_ratios = []

files = []
for dirname, dirnames, filenames in os.walk('./'):
    for f in filenames:
        matches = [ftr for ftr in filters if (ftr in f)]
        if len(matches) != len(filters):
            continue
        if "netstat" in f and ".txt" in f:
            files += [f]

descs = set()
values= []

for f in files:
    lines = open(f, 'r').readlines();
    values += [{"file":f, "values":{},"thput":0}]
    for l in lines:
        m = re.search('[0-9]+', l)
        if m:
            value = m.group(0)
            desc  = re.compile(r'([0-9]+|\s|\n|:)').sub('', l)
            descs.add(desc)
            values[-1]["values"][desc] = value
    lines = open(str.replace(f,'netstat','netperf'), 'r').readlines();
    values[-1]["thput"] = lines[-1].split()[-1]

print "Processing %d netstat attributes from %d files " % (len(descs),len(files))

# print values
#print descs

# print "#thput",
# for d in descs:
#     print d,
# print
# for v in values:
#     print v["thput"],
#     for d in descs:
#         write "%d" % int(v["values"][d] if v["values"].has_key(d) else 0),
#     print
xValues = []
for v in values:
    xValues += [float(v["thput"])]

for d in descs:
    plot = Plot()
    dots=Scatter()
    dots.label=("Correlation")
    dots.xValues=xValues
    
    dots.yValues=[]
    for v in values:
        dots.yValues += [int(v["values"][d] if v["values"].has_key(d) else 0)]
    plot.add(dots)
    plot.setTitle("%s" % str(d))
    plot.setYLabel("%s" % str(d))
    plot.setXLabel("Thput (mbps)")
    # plot.setAxesLabelSize(32)
    plot.save(output_dir+'/'+d+".png")
    
    

# xValues=[float(d[0][:-1]) for d in data[data.keys()[0]]]
# print filename
# # print xValues
# for k,v in data.items():
#     txLine=Line()
#     txLine.label=("%s TX" % k)


#     rxLine=Line()
#     rxLine.label=("%s RX" % k)

#     txLine.xValues = []
#     rxLine.xValues = []
#     txLine.xValues=xValues
#     rxLine.xValues=xValues
#     txLine.yValues = []
#     rxLine.yValues = []
#     print k
#     for i in range(1,len(v)):
#         #txLine.xValues+=[float(v[i][0][:-1])]
#         #rxLine.xValues+=[float(v[i][0][:-1])]
#         txLine.yValues+=[(int(v[i][1])-int(v[i-1][1]))*8/1e6]
#         rxLine.yValues+=[(int(v[i][2])-int(v[i-1][2]))*8/1e6]
#     #print txLine.yValues[:30]
#     if len(rxLine.yValues) == 0:
#         continue
#     plot.add(txLine)
#     plot.add(rxLine)


# #plot.setTitle(filename)
# plot.setYLabel("Bandwidth (Mbps)")
# plot.setXLabel("Time (s)")
# plot.setAxesLabelSize(32)
# plot.setXTickLabelSize(24)
# plot.setYTickLabelSize(24)
# plot.setYLimits(0,2000)
# plot.addLineColor("red")
# plot.addLineColor("blue")
# plot.addLineColor("green")
# plot.addLineColor("yellow")
# plot.addLineColor("purple")
# plot.addLineColor("brown")
# plot.addLineColor("gray")

# plot.addLineStyle("-")
# plot.addLineStyle("--")


# #plot.hasLegend(columns=4)
# plot.save(filename+".png")
