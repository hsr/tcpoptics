#!/usr/bin/env python
 
"""
SYNOPSIS
 
    Usage: ./nc-experiment.py [options]
 
DESCRIPTION
 
    Runs nc client and server for different transfer sizes and different 
    EPS/OCS parameters
 
"""

import subprocess
import optparse
import traceback
import sys

# FILE_SIZES    =[2**10,2**12,2**14,2**16] # In kb
# QUEUE_SIZES   =[32,256,1024,2048]           # In kb
# EPS_RATES     =[100,200,500]                # In mbps
# EPS_OCS_TIMES =[{"eps":1000, "ocs":1000},   # 1ms  x 1ms
#                 {"eps":10000,"ocs":10000},  # 10ms x 10ms
#                 {"eps":10000,"ocs":1000},   # 10ms x 1ms
#                 {"eps":1000, "ocs":10000},  # 1ms  x 10ms
#                 {"eps":10000, "ocs":100},   # 10ms x 100us
#                 {"eps":2000, "ocs":100},]   # 2ms  x 100us
# TCP_ALGORITHM =["cubic", "reno", "dctcp"]

FILE_SIZES    =[2**14] # In kb
QUEUE_SIZES   =[32]                # In kb
EPS_RATES     =[100]                # In mbps
EPS_OCS_TIMES =[{"eps":1000, "ocs":1000},   # 1ms  x 1ms
                {"eps":2000, "ocs":100},]   # 2ms  x 100us
# TCP_ALGORITHM =["cubic"]

def abort(msg):
    print msg
    sys.exit(1)

def warn(msg):
    print "WARNING:", msg

def run(cmd, bg=False, checkReturn=True):
    global options, args
    if options.verbose:
        print "cmd:", cmd
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, shell=True,
                             executable='/bin/bash')
        if bg:
            return
        output,stderr = p.communicate()
        if checkReturn and p.returncode:
            abort('unexpected return code %d for "%s".\n%s\n%s' % 
                  (p.returncode, cmd, output, stderr))        
        return output.strip()
    except Exception, e:
        None

def ssh(machine, cmd, user = 'root', bg=True, checkReturn=True):
    return run("ssh %s@%s '%s'" % (user, machine, cmd), 
               bg=bg, checkReturn=checkReturn);

def scp(machine, localfile, remotepath, user = 'root'):
    return run("scp %s %s@%s:%s" % (localfile, user, machine, remotepath))

# Assuming that:
#  - eth2 is already attached to vport 1 and represents EPS
#  - eth3 is already attached to vport 2 and represents OCS
def resetFlowRules(machine, eps_time, ocs_time):
    host = int(machine[-2:])
    cmds = "ovs-ofctl del-flows br0; \
            ovs-ofctl add-flow br0 \
              priority=1,arp,in_port=LOCAL,actions=output:2; \
            ovs-ofctl add-flow br0 \
              priority=0,arp,in_port=2,actions=output:65534; \
            ovs-ofctl add-flow br0 \
              priority=1,ip,nw_dst=10.55.55.%d,actions=output:65534; \
           ovs-ofctl add-flow br0 \
              priority=0,ip,actions=m_output:1-%d-2-%d;" % \
        (host,eps_time,ocs_time)
    ssh(machine, cmds, bg=False);

# Rates are in mbps
# TODO: extend this to support K on a ECN/RED qdisc
# @rate in mbps
# @qsize in kb
# @burst in kb
def setBWLimit(machine, rate=-1, qsize=4, burst=9):
    REMOVE_TB="tc qdisc del dev eth2 root &> /dev/null"
    ssh(machine, REMOVE_TB, bg=False, checkReturn=False);

    if rate > 0:
        cmds = "tc qdisc add dev eth2 root handle 1: htb default 255; \
                tc class add dev eth2 parent 1: classid 1:255 htb \
                   rate %dmbit ceil %dmbit burst %dkb; \
                tc qdisc add dev eth2 parent 1:255 bfifo limit %dkb" % \
            (rate, rate, burst, qsize)
        return ssh(machine, cmds, bg=False);
    return

def main():
    global options, FILE_SIZES, QUEUE_SIZES, EPS_RATES, EPS_OCS_TIMES

    # - Setup congestion control algorithm
    # - Create the file of a particular size
    # - Start nc server giving the file as input

    # - ssh to the client using eth0 and start the transfer (running
    #   netstat before and after the transmission)
    #
    # - store the parameters in an output file

    for fs in FILE_SIZES:
        for rate in EPS_RATES:
            for qs in QUEUE_SIZES:
                setBWLimit("d35",rate,qs);
                setBWLimit("d36",rate,qs);
                for interval in EPS_OCS_TIMES:
                    for i in range(0,10):
                        eps_time = int(interval["eps"]);
                        ocs_time = int(interval["ocs"]);

                        resetFlowRules("d35", eps_time, ocs_time);
                        resetFlowRules("d36", eps_time, ocs_time);

                        exp_name = "fs_%d_rate_%d_qs_%d_eps_%d_ocs_%d_i_%d" % \
                            (fs, rate, qs, eps_time, ocs_time, i)

                        run("./run-nc-server-with-netstat.sh %d %d %s" % \
                                (int(fs),4545,exp_name), bg=True)
                        ssh("d36", "./run-netcat-with-netstat.sh %s %s %s" % \
                                ("10.55.55.35", "4545", exp_name), user="hsr")
    return 0;

if __name__ == '__main__':
    try:
        
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')
        
        (options, args) = parser.parse_args()
        main()
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)

    


