#!/bin/bash

if [ ! -z "${1}" ] && [ ! -z "${2}" ] && [ ! -z "${3}" ]; then
	netstat -st > ${3}_old.txt
   dd if=/dev/zero bs=1024 count=${1} | nc -l ${2}
	netstat -st > ${3}_new.txt

	./netstat-tcp-parser.py ${3}_old.txt ${3}_new.txt > ${3}_netstat.txt
   rm ${3}_old.txt ${3}_new.txt
else
	echo "Missing parameters!"
	echo "Usage: $0 <transfer-size> <port> <output prefix>"
fi
