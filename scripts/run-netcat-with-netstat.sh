#!/bin/bash

if [ ! -z "$1" ] && [ ! -z "$2" ] && [ ! -z "$3" ]; then

	netstat -st > $3_old.txt
   (time netcat $1 $2 &>/dev/null) 2> $3_time.txt
	netstat -st > $3_new.txt

	./netstat-tcp-parser.py $3_old.txt $3_new.txt > $3_netstat.txt
   rm $3_old.txt $3_new.txt
else
	echo "Missing parameters!"
	echo "Usage: $0 <ip> <port> <output prefix>"
fi
