#!/usr/bin/env python
 
"""
SYNOPSIS
 
    Usage: ./link-control.py <interface> [options]
 
DESCRIPTION
 
    Given an interface name, a link speed and intervals in us, this script
    install token buckets at the specified interface configured with the
    desired link speeds at a frequency that matches the given intervals.
 
"""

import subprocess
import optparse
import traceback
import sys
import time
from datetime import datetime as dt

usleep = lambda x: time.sleep(x/1.0e6)

def abort(msg):
    print msg;
    sys.exit(1)

def warn(msg):
    print "WARNING:", msg

def run(cmd, checkReturnCode=True):
    global options, args
    
    if options.verbose:
        print "cmd:", cmd
    
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, shell=True,
                             executable='/bin/bash')
        output,stderr = p.communicate()
        if checkReturnCode and p.returncode:
            abort('unexpected return code %d for "%s".\n%s\n%s' % 
                  (p.returncode, cmd, output, stderr))
        return output.strip()
    except Exception, e:
        None

def throttle(iface, rate, burst, limit):
    run("sudo tc qdisc add dev %s root handle 1: \
           tbf rate %dmbit burst %dkb limit %d" % 
        (iface, rate, burst, limit));

def unthrottle(iface, may_fail = False):
    run("sudo tc qdisc del dev %s root" % (iface), may_fail);

def main():
    global options

    ai    = float(options.active_interval)
    ii    = float(options.inactive_interval)
    tt    = int(options.total_time)
    iface = str(options.iface)
    rate  = options.rate
    burst = options.burst
    limit = options.limit

    unthrottle(iface)

    now = start = dt.now()
    try: 
        while (now-start).seconds < tt:
            now   = dt.now()
            throttle(iface, rate, burst, limit)
            e     = dt.now() - now
            wait4 = ai - e.seconds*1.0e6 - (e.microseconds)
            if wait4 < 0:
                warn("throttle() is taking more than %.2fus to run!" % ai)
                wait4 = 0
            else:
                usleep(wait4)

            now = dt.now()
            unthrottle(iface)
            e     = dt.now() - now
            wait4 = ii - e.seconds*1e6 - (e.microseconds)
            if wait4 < 0:
                warn(" unthrottle() is taking more than %.2fus to run!" % ii)
                wait4 = 0
            else:
                usleep(wait4)

            now = dt.now()
    except KeyboardInterrupt, e: # Ctrl-C
        None
    
    return 0;

if __name__ == '__main__':
    try:        
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')
        
        parser.add_option('-a', '--active_interval', action='store',
                          default='1000', type="int",
                          help='Interval that the link remains throttled (in us)')

        parser.add_option('-i', '--inactive_interval', action='store',
                          default='10000', type="int",
                          help='Interval that the link isn\'t throttled (in us)')

        parser.add_option('-r', '--rate', action='store',
                          default='100', type="int",
                          help='Speed of the link when it is throttled (in mbps)')

        parser.add_option('-b', '--burst', action='store',
                          default='18', type="int",
                          help='Allowed burst when link is throttled (in kb)')

        parser.add_option('-l', '--limit', action='store',
                          default='90000', type="int",
                          help='Size of the queue (in bytes)')

        parser.add_option('-t', '--total_time', action='store',
                          default='10', type="int",
                          help='Total execution time of this script (in s)')
        

        if len(sys.argv) > 1:
            (options, args) = parser.parse_args(args=sys.argv[1:])
            options.iface = sys.argv[1]
            sys.exit(main())
        else:
            parser.print_usage()

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
